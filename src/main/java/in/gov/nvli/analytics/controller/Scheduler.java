/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.analytics.controller;

/**
 *
 * @author Ankita Dhongde <dankita@cdac.in>
 */
import in.gov.nvli.analytics.mongodb.domain.ActivityLog;
import in.gov.nvli.analytics.mongodb.domain.Record;
import in.gov.nvli.analytics.mongodb.domain.RecordShareCollection;
import in.gov.nvli.analytics.mongodb.domain.SearchKeyword;
import in.gov.nvli.analytics.mongodb.domain.TrendingSearch;
import in.gov.nvli.analytics.mongodb.domain.TrendingSearchCollection;
import in.gov.nvli.analytics.service.ActivityLogMiningService;
import in.gov.nvli.analytics.service.ResourceInfoService;
import in.gov.nvli.analytics.util.Constants;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.TypedAggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import static org.springframework.data.domain.Sort.Direction.DESC;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.limit;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.unwind;

@Service
public class Scheduler {
    @Autowired
    public MongoTemplate mongoTemplate;

    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    ActivityLogMiningService logMiningService;

    @Autowired
    ResourceInfoService resourceInfoService;

    public Integer counter;

    final String SHARE_LAST_24H = "recordLast24Hr";

    final String SHARE_LAST_1M7D = "recordLast1M7D";

    final String SHARE_LAST_1Y = "recordLast1Year";

    final long millisInDay = 86400000L, millisInHour = 3600000L;

    @PostConstruct
    public void runAtStartup() {
        getMostSearchedKeywords();
        getTop10SharedRecod24h();
        getTop10SharedRecod1M7D();
        getTop10SharedRecod1Year();
    }

    /**
     * Schedular to be run every hour to generate statactics of share
     * record for last 24 hour.
     */
    @Scheduled(cron = "0 0 * * * *")
    public void getTop10SharedRecod24h() {
        long now = System.currentTimeMillis();
        long startTime = now - millisInDay;
        long timeStep = millisInHour;
        PerformAggregationForShareActivity(startTime,
                now, timeStep, SHARE_LAST_24H,
                Arrays.asList(Constants.UserActivityConstant.INTRA_SHARE_RECORD,
                        Constants.UserActivityConstant.SOCIAL_SHARE_RECORD));
    }

    @Scheduled(cron = "0 1 0 * * *")
    public void getTop10SharedRecod1M7D() {
        long now = System.currentTimeMillis();
        long startTime;
        startTime = now - (millisInDay * 30L);
        long timeStep = millisInDay;
        PerformAggregationForShareActivity(startTime,
                now, timeStep, SHARE_LAST_1M7D,
                Arrays.asList(Constants.UserActivityConstant.INTRA_SHARE_RECORD,
                        Constants.UserActivityConstant.SOCIAL_SHARE_RECORD));

    }
    @Scheduled(cron = "0 1 0 1 * *")
    public void getTop10SharedRecod1Year() {
        long now = System.currentTimeMillis();
        long startTime = now - (millisInDay * 30L * 12L);
        long timeStep = millisInDay * 30L;
        PerformAggregationForShareActivity(startTime,
                now, timeStep, SHARE_LAST_1Y,
                Arrays.asList(Constants.UserActivityConstant.INTRA_SHARE_RECORD,
                        Constants.UserActivityConstant.SOCIAL_SHARE_RECORD));
    }

    private void PerformAggregationForShareActivity(long startTime, long now,
            long timeStep, String collectionName, List<String> activityTypes) {
        mongoTemplate.dropCollection(collectionName);
        mongoTemplate.createCollection(collectionName);
        HashMap<String, Object> trendList = new HashMap<>();
        List recordIdentifiers = new ArrayList<>();
        TypedAggregation<ActivityLog> aggregation = newAggregation(
                ActivityLog.class,
                match(Criteria.where("activity_type").in(activityTypes)),
                match(Criteria.where("activity.activity_ts").gte(new Date(startTime))),
                match(Criteria.where("activity.activity_ts").lte(new Date(now))),
                group("recordIdentifier").count().as("count"),
                project("count").and("recordIdentifier").previousOperation(),
                sort(DESC, "count"),
                limit(10)
        );
        AggregationResults<Record> results;
        results = mongoTemplate.aggregate(aggregation, Record.class);
        List<Record> mappedRecords = results.getMappedResults();
        for (Record mappedRecord : mappedRecords) {
            trendList.put(mappedRecord.getRecordIdentifier(), getShareTrendValues(mappedRecord.getRecordIdentifier(), startTime, now, timeStep));
            recordIdentifiers.add(mappedRecord.getRecordIdentifier());
        }
        RecordShareCollection recordShareCollection = new RecordShareCollection(mappedRecords, trendList, resourceInfoService.getResourceInfo(recordIdentifiers));
        mongoTemplate.insert(recordShareCollection, collectionName);
        Map<String, Object> outputMap = new HashMap<>();
        outputMap.put("data", mappedRecords);
        outputMap.put("trendsData", trendList);
        outputMap.put("info", resourceInfoService.getResourceInfo(recordIdentifiers));
        redisTemplate.opsForHash().putAll(collectionName, outputMap);
    }

    /**
     * Schedular to be run every hour to generate statactics of search
     *
     */
    @Scheduled(cron = "0 0 */3 * * *")
    public void getMostSearchedKeywords() {
        redisTemplate.opsForValue().setIfAbsent("lastAnalysedActivityTime", Instant.ofEpochMilli(0).toEpochMilli());
        String sd = redisTemplate.opsForValue().getAndSet("lastAnalysedActivityTime", new Date().getTime()).toString().replaceAll("\"", "");
        Date lastAnalysedActivityTime = new Date(Long.valueOf(sd));
        try {
            Aggregation aggregation = newAggregation(ActivityLog.class,
                    match(Criteria.where("activity_type").is(Constants.UserActivityConstant.KEYWORD_SEARCHED)),
                    match(Criteria.where("activity.activity_ts").gte(lastAnalysedActivityTime)),
                    unwind("activity.tokanizedPhrase"), group("activity.tokanizedPhrase").count().as("score"),
                    sort(Sort.Direction.DESC, "score"), Aggregation.limit(50));
            AggregationResults results = mongoTemplate.aggregate(aggregation, "activityLog", SearchKeyword.class);
            List<SearchKeyword> listOfMSK = results.getMappedResults();
            for (SearchKeyword msk : listOfMSK) {
                redisTemplate.opsForZSet().incrementScore("mostSearchedKeywords", msk.getId(), msk.getScore());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Scheduled(cron = "0 1 * * * *")
    public void getSearchTrendsBetween() {
        long now = System.currentTimeMillis();
        Date endTime = new Date(now);
        Date startTime = new Date(now - 86400000L);//past 24 hour
        Aggregation aggregations = Aggregation.newAggregation(
                match(Criteria.where("activity.activity_ts").gte(startTime)),
                match(Criteria.where("activity.activity_ts").lte(endTime)),
                match(Criteria.where("activity.searchPhrase").ne("")),
                match(Criteria.where("activity_type").is("userSearchPhrases")),
                group("activity.searchPhrase").count().as("count").last("activity.permaLink").as("permaLink"),
                project("count", "permaLink").and("searchPhrase").previousOperation(),
                sort(DESC, "count"),
                limit(10)
        );
        AggregationResults<TrendingSearch> results = mongoTemplate.aggregate(aggregations, Constants.CollectionNames.USER_ACTIVITY_LOG, TrendingSearch.class);
        List<TrendingSearch> mappedRecords = results.getMappedResults();
        HashMap<String, List<Long>> trendList = new HashMap<>();
        for (TrendingSearch mappedRecord : mappedRecords) {
            trendList.put(mappedRecord.getSearchPhrase(), getTrendValues(mappedRecord.getSearchPhrase(), now));
        }
        TrendingSearchCollection trendingSearchCollection = new TrendingSearchCollection(mappedRecords, trendList);
        mongoTemplate.save(trendingSearchCollection);
    }

    public List<Long> getTrendValues(String item, long endTime) {
        long startTime = endTime - 86400000L;
        List<Long> trends = new ArrayList<>();
        for (long i = startTime, j = i + 3600000L; i < endTime; i += 3600000L, j += 3600000L) {
            Aggregation aggregations = Aggregation.newAggregation(
                    match(Criteria.where("activity.activity_ts").gte(new Date(i))),
                    match(Criteria.where("activity.activity_ts").lte(new Date(j))),
                    match(Criteria.where("activity.searchPhrase").is(item)),
                    match(Criteria.where("activity_type").is("userSearchPhrases")),
                    group("activity.searchPhrase").count().as("count"),
                    project("count")
            );
            AggregationResults<TrendingSearch> results = mongoTemplate.aggregate(aggregations, Constants.CollectionNames.USER_ACTIVITY_LOG, TrendingSearch.class);
            List<TrendingSearch> mappedRecords = results.getMappedResults();

            if (mappedRecords.size() > 0) {
                trends.add((long) mappedRecords.get(0).getCount());
            } else {
                trends.add(0L);
            }
        }
        return trends;
    }

    public HashMap<String, Object> getShareTrendValues(String item, long startTime, long endTime, long timeStep) {
        HashMap<String, Object> hashMap = new HashMap<>();
        List<Long> trends = new ArrayList<>();
        List<Long> times = new ArrayList<>();
        for (long i = startTime, j = i + timeStep; i < endTime; i += timeStep, j += timeStep) {
            Aggregation aggregations = Aggregation.newAggregation(
                    match(Criteria.where("activity_type").in(Constants.UserActivityConstant.INTRA_SHARE_RECORD, Constants.UserActivityConstant.SOCIAL_SHARE_RECORD)),
                    match(Criteria.where("activity.activity_ts").gte(new Date(i))),
                    match(Criteria.where("activity.activity_ts").lte(new Date(j))),
                    match(Criteria.where("recordIdentifier").is(item)),
                    group("recordIdentifier").count().as("count"),
                    project("count")
            );
            AggregationResults<Record> results = mongoTemplate.aggregate(aggregations, Constants.CollectionNames.USER_ACTIVITY_LOG, Record.class);
            List<Record> mappedRecords = results.getMappedResults();
            times.add(i);
            if (mappedRecords.size() > 0) {
                trends.add((long) mappedRecords.get(0).getCount());
            } else {
                trends.add(0L);
            }

        }
        hashMap.put("time", times);
        hashMap.put("trends", trends);
        return hashMap;
    }
}
