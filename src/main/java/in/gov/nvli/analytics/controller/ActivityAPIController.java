package in.gov.nvli.analytics.controller;

import in.gov.nvli.analytics.mongodb.domain.ActivityLog;
import in.gov.nvli.analytics.mongodb.service.ActivityLogService;
import in.gov.nvli.analytics.service.ActivityLogMiningService;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author akinza
 */
@RestController
@RequestMapping(value = "/activityAPI")
public class ActivityAPIController {

    @Autowired
    ActivityLogService activityLogService;

    @Autowired
    ActivityLogMiningService logMiningService;

    @CrossOrigin
    @RequestMapping(value = "/logins", method = RequestMethod.GET)
    public List<ActivityLog> logins(
            @RequestParam(value = "startTime") long startTime,
            @RequestParam(value = "endTime") long endTime
    ) {

        Date startDateTime = new Date(startTime);
        Date endDateTime = new Date(endTime);
//        return activityLogService.getLoginActivityBetween(startDateTime, endDateTime);
        return activityLogService.getLogins(startDateTime, endDateTime);
    }

    @CrossOrigin
    @RequestMapping(value = "/top/shared/record/{t}", method = RequestMethod.GET)
    public Map<String, Object> topSharedRecords(
            @PathVariable(value = "t") String timeRange
    ) {
        return logMiningService.findTop10SharedRecord(timeRange);
    }

    @CrossOrigin
    @RequestMapping(value = "/trending/search", method = RequestMethod.GET)
    public Map<String, Object> searchTrends() {
        return logMiningService.fetchSearchTrendsBetween();
    }
}
