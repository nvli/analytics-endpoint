/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.analytics.controller;

import com.google.gson.JsonObject;
import com.mongodb.BasicDBObject;
import in.gov.nvli.analytics.mongodb.domain.CustomEntity;
import in.gov.nvli.analytics.mongodb.domain.Person;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Ruturaj
 */
@RequestMapping("/entity")
@Controller
public class EntityExtractionController {

    private ObjectId lastId;


    @Value("${mysql.host.address}")
    private String mysqlHost;

    @Value("${mysql.port}")
    private String mysqlPort;

    @Value("${mysql.user}")
    private String mysqlUser;

    @Value("${mysql.password}")
    private String mysqlPass;

    @Value("${mysql.database.name}")
    private String dbName;

    @Autowired
    private MongoTemplate mongoTemplate;

    final static Logger logger = LoggerFactory.getLogger(EntityExtractionController.class);
//    private ObjectId lastId;

    @RequestMapping("/convert")
    @ResponseBody
    public void MysqlToNosql() {
        JsonObject object = new JsonObject();
        object.addProperty("success", "success");
        List<Person> persons = new ArrayList<>();
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection con = DriverManager.getConnection("jdbc:mysql://" + mysqlHost + "/" + dbName, mysqlUser, mysqlPass);

            Statement st = con.createStatement();
            String sql = ("SELECT * FROM record_category ORDER BY id;");
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Person person = new Person();
                int id = rs.getInt("id");
                System.out.println("idd " + id);
                String query = "select * from linked_records where records=?";
                PreparedStatement p = con.prepareStatement(query);
                p.setInt(1, id);
                ResultSet rs2 = p.executeQuery();
                while (rs2.next()) {
                    int name_id = rs2.getInt("name_id");
                    String query2 = "select * from names_table where name_id=?";
                    PreparedStatement p2 = con.prepareStatement(query2);
                    p2.setInt(1, name_id);
                    ResultSet rs3 = p2.executeQuery();
                    while (rs3.next()) {
                        rs3.getInt("name_id");
                        List<String> li = new ArrayList<>();
                        li.add(rs3.getString("names"));
                        person.setPersons(li);
                    }
                }
                person.setRecordName(rs.getString("record_name"));
                int catId = rs.getInt("category");
                switch (catId) {
                    case 1:
                        person.setCategory("Rare Books");
                        person.setResourceCode("RABK");
                        break;
                    case 2:
                        person.setCategory("Private Archives Papers");
                        person.setResourceCode("PARC");
                        break;
                    case 3:
                        person.setCategory("Public Records");
                        person.setResourceCode("PUBR");
                        break;
                    case 4:
                        person.setCategory("Open Repository");
                        person.setResourceCode("OPRE");
                        break;
                }
                persons.add(person);
            }
            con.close();

            if (!mongoTemplate.collectionExists("namesCollection")) {
                mongoTemplate.createCollection("namesCollection");
            }

            for (Person person : persons) {
                BasicDBObject basicDBObject = new BasicDBObject();
                basicDBObject.append("category", person.getCategory());
                basicDBObject.append("resourceCode", person.getResourceCode());
                basicDBObject.append("recordName", person.getRecordName());
                basicDBObject.append("persons", person.getPersons());
                mongoTemplate.save(basicDBObject, "namesCollection");

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping("/extractPersonEntity")
    @ResponseBody
    public void extractPersonEntity() {
        List<String> list = new ArrayList<>();
        System.out.println("@@@ " + mongoTemplate.getCollection("namesCollection"));
        Query query1 = new Query();
        List<Person> employees = mongoTemplate.find(query1, Person.class, "namesCollection");
        System.out.println("employees " + employees.size());
        for (Person employee : employees) {
            if (employee.getPersons() == null) {
                System.out.println("null");
                System.out.println("@@ " + employee.getRecordName());
                continue;
            }

            for (String user : employee.getPersons()) {
                if (!list.contains(user)) {
                    System.out.println("added");
                    list.add(user);
                    continue;
                } else {
                    CustomEntity entity = new CustomEntity();
                    entity.setEntityName(user);
                    entity.setEntityType("Person");

                    System.out.println("name=" + user);
                    Query query = new Query();
                    query.addCriteria(Criteria.where(user).in("persons"));
                    List<Person> result = mongoTemplate.find(query, Person.class);
                    entity.setFrequency((long) result.size());
                    List<String> records = new ArrayList<>();
                    for (Person person : result) {
                        System.out.println("result " + person.getRecordName());
                        records.add(person.getRecordName());
                    }
                    entity.setRecords(records);
                    mongoTemplate.save(entity);
                }

            }
        }

    }

    @RequestMapping(value = "/getPersonNamesEntity/{pageNumber}", method = {RequestMethod.GET})
    @ResponseBody
    public List<CustomEntity> getPersonNamesEntity(@PathVariable("pageNumber") int pageNumber, HttpServletRequest request) {
        List<CustomEntity> list = new ArrayList<>();


        HttpSession session = request.getSession();
        if (pageNumber == 1) {
            Query query = new Query();
            query.limit(25);
            list = mongoTemplate.find(query, CustomEntity.class, "entities");
            logger.info("SIZE OF ENTITIES >>>> IF " + list.size());
            int size = list.size() - 1;
            lastId = list.get(size).getId();
            session.setAttribute("lastId", lastId);
        } else {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").gt(lastId));
            query.limit(25);
            list = mongoTemplate.find(query, CustomEntity.class, "entities");
            int size = list.size() - 1;
            lastId = list.get(size).getId();
        }
        return list;
    }

    private TaskScheduler scheduler = new ConcurrentTaskScheduler();

    @PostConstruct
    private void executeJob() {
        scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                // your business here
            }
        }, 20000000);
    }
}
