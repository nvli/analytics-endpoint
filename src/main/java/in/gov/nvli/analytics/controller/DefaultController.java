/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.analytics.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author akinza
 */
@Controller

public class DefaultController {
//    @Autowired
//    Scheduler scheduler;

    @RequestMapping(value = {"/", ""})
    public ModelAndView index(){
        ModelAndView mav = new ModelAndView();
        mav.setViewName("index");
        return mav;
    }

    @RequestMapping(value = {"/test"})
    public ModelAndView test() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("index");
        //   scheduler.getTop10SharedRecod1M7D();
        return mav;
    }

}
