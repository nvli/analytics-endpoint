/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.analytics.mongodb.domain;

import java.io.Serializable;

/**
 *
 * @author ruturaj
 */

public class SearchKeyword implements Serializable {
    private int score;
    private String id;

    public SearchKeyword() {
    }

    public SearchKeyword(int score, String id) {
        this.score = score;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "SearchKeyword{" + "score=" + score + ", id=" + id + '}';
    }


}
