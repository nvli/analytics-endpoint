package in.gov.nvli.analytics.mongodb.domain;

import java.io.Serializable;

/**
 *
 * @author Saurabh Koriya <ksaurabh@cdac.in>
 */
public class UserNvliShare extends Activity implements Serializable {

    private String message;
    private String recordIdentifier;
    private Long sharedWithUser;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    public Long getSharedWithUser() {
        return sharedWithUser;
    }

    public void setSharedWithUser(Long sharedWithUser) {
        this.sharedWithUser = sharedWithUser;
    }
}
