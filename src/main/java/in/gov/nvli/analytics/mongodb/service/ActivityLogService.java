package in.gov.nvli.analytics.mongodb.service;
import in.gov.nvli.analytics.mongodb.domain.ActivityLog;
import in.gov.nvli.analytics.mongodb.repositories.ActivityLogRepository;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
/**
 *
 * @author Saurabh Koriya <ksaurabh@cdac.in>
 */
@Service
public class ActivityLogService implements IActivityService<ActivityLog> {

    @Autowired
    MongoOperations mongoOperations;
    
    @Autowired
    MongoTemplate mongoTemplate;
    
    @Autowired
    ActivityLogRepository activityLogRepository;

    @Override
    public List<ActivityLog> getLogins(Date startTime, Date endTime){
        return activityLogRepository.findByActivityTypeAndActivityActivityTsBetween("login", startTime, endTime);
    }
    
    @Override
    public List<ActivityLog> getLoginActivityBetween(Date startTime, Date endTime){

        Query query = new Query();
        query.addCriteria(Criteria.where("activity").elemMatch(Criteria.where("activityTs").lte(endTime).and("activityTs").gte(startTime)));
        
        System.out.println("QUERY " + query);
        List<ActivityLog> loginActivities = mongoTemplate.find(query,ActivityLog.class);
        
        return loginActivities;
    }
}
