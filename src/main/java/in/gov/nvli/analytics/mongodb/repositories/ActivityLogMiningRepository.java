package in.gov.nvli.analytics.mongodb.repositories;

import in.gov.nvli.analytics.mongodb.domain.TopSharedResource;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 * @author akinza
 */
public interface ActivityLogMiningRepository extends MongoRepository<TopSharedResource, ObjectId> {

    
}
