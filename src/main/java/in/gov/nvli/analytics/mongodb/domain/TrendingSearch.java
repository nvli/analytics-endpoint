package in.gov.nvli.analytics.mongodb.domain;

import java.io.Serializable;

/**
 *
 * @author akinza
 */
public class TrendingSearch implements Serializable{
    String searchPhrase;
    String permaLink;
    int count;

    public String getSearchPhrase() {
        return searchPhrase;
    }

    public void setSearchPhrase(String searchPhrase) {
        this.searchPhrase = searchPhrase;
    }


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getPermaLink() {
        return permaLink;
    }

    public void setPermaLink(String permaLink) {
        this.permaLink = permaLink;
    }


}
