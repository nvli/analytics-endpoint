package in.gov.nvli.analytics.mongodb.domain;

import java.io.Serializable;

/**
 *
 * @author Saurabh Koriya <ksaurabh@cdac.in>
 */
public class UserSocialShare extends Activity implements Serializable {

    private static final long serialVersionUID = 1L;
    private String recordIdentifier;
    private SocialSite socialSiteBean;

    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    public SocialSite getSocialSiteBean() {
        return socialSiteBean;
    }

    public void setSocialSiteBean(SocialSite socialSiteBean) {
        this.socialSiteBean = socialSiteBean;
    }
}