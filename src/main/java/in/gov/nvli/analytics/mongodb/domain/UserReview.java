package in.gov.nvli.analytics.mongodb.domain;

import java.io.Serializable;

/**
 *
 * @author Saurabh Koriya <ksaurabh@cdac.in>
 */
public class UserReview extends Activity implements Serializable {

    private static final long serialVersionUID = 1L;
    private String recordIdentifier;
    private String reviewText;
    private boolean old;
    private boolean latest;
    private String status;

    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    public String getReviewText() {
        return reviewText;
    }

    public void setReviewText(String reviewText) {
        this.reviewText = reviewText;
    }

    public boolean isOld() {
        return old;
    }

    public void setOld(boolean old) {
        this.old = old;
    }

    public boolean isLatest() {
        return latest;
    }

    public void setLatest(boolean latest) {
        this.latest = latest;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}