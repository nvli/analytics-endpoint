package in.gov.nvli.analytics.mongodb.repositories;

import in.gov.nvli.analytics.mongodb.domain.ActivityLog;
import java.util.Date;
import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 *
 * @author Saurabh Koriya <ksaurabh@cdac.in>
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
public interface ActivityLogRepository extends MongoRepository<ActivityLog, ObjectId> {

    @Query(value = "{'activity_type':?0, 'activity.activity_ts':{ $gte:?1, $lte:?2 }}")
    public List<ActivityLog> findByActivityTypeAndActivityActivityTsBetween(String activityType, Date startDate, Date endTime);
 
}
