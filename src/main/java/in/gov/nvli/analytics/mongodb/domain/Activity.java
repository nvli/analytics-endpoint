package in.gov.nvli.analytics.mongodb.domain;
import java.io.Serializable;
import java.util.Date;
import org.springframework.data.mongodb.core.mapping.Field;
/**
 *
 * @author Saurabh Koriya <ksaurabh@cdac.in>
 */
public class Activity implements Serializable {

    @Field(value = "activity_ts")
    Date activityTs;
    Long affectedUserId;

    public Long getAffectedUserId() {
        return affectedUserId;
    }

    public void setAffectedUserId(Long affectedUserId) {
        this.affectedUserId = affectedUserId;
    }

    public Date getActivityTs() {
        return activityTs;
    }

    public void setActivityTs(Date activityTs) {
        this.activityTs = activityTs;
    }
}