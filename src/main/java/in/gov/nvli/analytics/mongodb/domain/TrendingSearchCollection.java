/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.analytics.mongodb.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author Sanjay
 */
@Document(collection = "TrendingSearch")
public class TrendingSearchCollection implements Serializable {

    List<TrendingSearch> mappedRecords ;
    Map<String, List<Long>> trendList;

    public TrendingSearchCollection() {
    }

    public TrendingSearchCollection(List<TrendingSearch> mappedRecords, Map<String, List<Long>> trendList) {
        this.mappedRecords = mappedRecords;
        this.trendList = trendList;
    }


    public List<TrendingSearch> getMappedRecords() {
        return mappedRecords;
    }

    public void setMappedRecords(List<TrendingSearch> mappedRecords) {
        this.mappedRecords = mappedRecords;
    }

    public Map<String, List<Long>> getTrendList() {
        return trendList;
    }

    public void setTrendList(Map<String, List<Long>> trendList) {
        this.trendList = trendList;
    }

}
