/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.analytics.mongodb.domain;

import java.util.List;
import java.util.Map;

/**
 *
 * @author Ruturaj
 */
public class RecordShareCollection {

    List<? extends Record> mappedRecords;

    Map<String, Object> trendList;

    Object info;

    public RecordShareCollection() {
    }

    public RecordShareCollection(List<? extends Record> mappedRecords, Map<String, Object> trendList, Object info) {
        this.mappedRecords = mappedRecords;
        this.trendList = trendList;
        this.info = info;
    }

    public List<? extends Record> getMappedRecords() {
        return mappedRecords;
    }

    public void setMappedRecords(List<? extends Record> mappedRecords) {
        this.mappedRecords = mappedRecords;
    }

    public Map<String, Object> getTrendList() {
        return trendList;
    }

    public void setTrendList(Map<String, Object> trendList) {
        this.trendList = trendList;
    }

    public Object getInfo() {
        return info;
    }

    public void setInfo(Object info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "RecordShareCollection{" + "mappedRecords=" + mappedRecords + ", trendList=" + trendList + ", info=" + info + '}';
    }

}
