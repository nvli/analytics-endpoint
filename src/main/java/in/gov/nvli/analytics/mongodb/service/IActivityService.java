package in.gov.nvli.analytics.mongodb.service;

import in.gov.nvli.analytics.mongodb.domain.ActivityLog;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Saurabh Koriya <ksaurabh@cdac.in>
 * @author Sanjay Rabidas <sanjayr@cdac.in, rabidassanjay@gmail.com>
 * @param <T>
 */
public interface IActivityService<T> {
    public List<ActivityLog> getLogins(Date startTime, Date endTime);
    public List<ActivityLog> getLoginActivityBetween(Date startTime, Date endTime);
}
