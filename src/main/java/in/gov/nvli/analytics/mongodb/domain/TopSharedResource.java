package in.gov.nvli.analytics.mongodb.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author akinza
 */
public class TopSharedResource implements Serializable{
    Date startTime, endTime;
    List<Record> records;

    public TopSharedResource(Date startTime, Date endTime, List<Record> records) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.records = records;
    }

    public TopSharedResource() {
    }

    
    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public List<Record> getRecords() {
        return records;
    }

    public void setRecords(List<Record> records) {
        this.records = records;
    }

    
}
