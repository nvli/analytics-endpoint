package in.gov.nvli.analytics.mongodb.domain;

import java.io.Serializable;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

/**
 * The persistent class for the user_search_phrase database table.
 *
 * @author Priya More<mpriya@cdac.in>
 */
public class UserSearchPhrase extends Activity implements Serializable, Comparable<UserSearchPhrase> {

    private static final long serialVersionUID = 1L;
    @Id
    private ObjectId userSearchPhraseId;
    private String searchPhrase;
    private String permaLink;
    //1=saved Search,0-not saved
    private int savedFlag;
    private String recordIdentifier;

    public UserSearchPhrase() {
        this.userSearchPhraseId = ObjectId.get();
    }

    public String getPermaLink() {
        return permaLink;
    }

    public void setPermaLink(String permaLink) {
        this.permaLink = permaLink;
    }

    public int getSavedFlag() {
        return savedFlag;
    }

    public void setSavedFlag(int savedFlag) {
        this.savedFlag = savedFlag;
    }

    public String getSearchPhrase() {
        return searchPhrase;
    }

    public void setSearchPhrase(String searchPhrase) {
        this.searchPhrase = searchPhrase;
    }

    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    public ObjectId getUserSearchPhraseId() {
        return userSearchPhraseId;
    }

    public void setUserSearchPhraseId(ObjectId userSearchPhraseId) {
        this.userSearchPhraseId = userSearchPhraseId;
    }

    @Override
    public int compareTo(UserSearchPhrase t) {
        return t.activityTs.compareTo(this.activityTs);
    }
}
