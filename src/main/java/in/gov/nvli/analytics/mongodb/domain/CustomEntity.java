/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.analytics.mongodb.domain;

import java.util.List;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 *
 * @author Ruturaj
 */
@Entity(value = "entities", noClassnameStored = true)
public class CustomEntity {

    @Id
    private ObjectId id;

    private String entityName;

    private String entityType;

    private long frequency;

    List<String> records;

    public CustomEntity() {
    }

    public CustomEntity(String entityName, String entityType, long frequency, List<String> records) {
        this.entityName = entityName;
        this.entityType = entityType;
        this.frequency = frequency;
        this.records = records;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public long getFrequency() {
        return frequency;
    }

    public void setFrequency(long frequency) {
        this.frequency = frequency;
    }

    public List<String> getRecords() {
        return records;
    }

    public void setRecords(List<String> records) {
        this.records = records;
    }

}
