package in.gov.nvli.analytics.mongodb.domain;
import java.io.Serializable;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Field;
/**
 *
 * @author Saurabh Koriya <ksaurabh@cdac.in>
 */
public class SearchActivityLog implements Serializable{

    @Id 
    ObjectId _id;
    @Indexed
    Long userId;
    @Field(value = "activity_type")
    String activityType;
    private String ipAddress;
    private String userAgentInfo;
    private String recordIdentifier;
    UserSearchPhrase userSearchPhrase;

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public ObjectId getId() {
        return _id;
    }

    public void setId(ObjectId _id) {
        this._id = _id;
    }

    public UserSearchPhrase getUserSearchPhrase() {
        return userSearchPhrase;
    }

    public void setUserSearchPhrase(UserSearchPhrase userSearchPhrase) {
        this.userSearchPhrase = userSearchPhrase;
    }

    
    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getUserAgentInfo() {
        return userAgentInfo;
    }

    public void setUserAgentInfo(String userAgentInfo) {
        this.userAgentInfo = userAgentInfo;
    }

    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }
}
