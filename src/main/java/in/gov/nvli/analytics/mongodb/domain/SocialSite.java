package in.gov.nvli.analytics.mongodb.domain;
import java.io.Serializable;
import org.springframework.data.mongodb.core.mapping.Field;
/**
 *
 * @author Saurabh Koriya <ksaurabh@cdac.in>
 */
public class SocialSite implements Serializable {

    private static final long serialVersionUID = 1L;
    @Field(value = "social_site_id")
    private Integer socialSiteId;
    @Field(value = "site_name")
    private String siteName;
    @Field(value = "provider_id")
    private String providerId;

    public Integer getSocialSiteId() {
        return socialSiteId;
    }

    public void setSocialSiteId(Integer socialSiteId) {
        this.socialSiteId = socialSiteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }
}