/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.analytics.service;

import in.gov.nvli.analytics.util.Helper;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in, rabidassanjay@gmail.com>
 */
@Service
public class ResourceInfoService {

    @Value("${ws.base.url}")
    String mitWebServiceBaseURL;
    public Object getResourceInfo(List recordIdentifiers) {
        HttpEntity<String> httpEntity = new HttpEntity(recordIdentifiers, Helper.mitWebserviceAthentication());
        RestTemplate template = new RestTemplate();
        String url = mitWebServiceBaseURL + "/record/information";
        ResponseEntity responseEntity = template.postForEntity(url, httpEntity, Map.class);
        return responseEntity.getBody();
    }
}
