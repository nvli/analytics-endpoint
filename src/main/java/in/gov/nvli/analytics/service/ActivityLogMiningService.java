package in.gov.nvli.analytics.service;

import in.gov.nvli.analytics.controller.Scheduler;
import in.gov.nvli.analytics.mongodb.domain.ActivityLog;
import in.gov.nvli.analytics.mongodb.domain.Record;
import in.gov.nvli.analytics.mongodb.domain.RecordShareCollection;
import in.gov.nvli.analytics.mongodb.domain.TopSharedResource;
import in.gov.nvli.analytics.mongodb.domain.TrendingSearchCollection;
import in.gov.nvli.analytics.mongodb.repositories.ActivityLogMiningRepository;
import in.gov.nvli.analytics.mongodb.repositories.ActivityLogRepository;
import in.gov.nvli.analytics.util.Constants;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.TypedAggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import static org.springframework.data.domain.Sort.Direction.DESC;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.limit;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;

/**
 *
 * @author akinza
 */
@Service
public class ActivityLogMiningService {

    @Autowired
    MongoOperations mongoOperations;

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    ActivityLogRepository activityLogRepository;

    @Autowired
    ActivityLogMiningRepository miningRepository;

    @Autowired
    ResourceInfoService resourceInfoService;

    @Autowired
    Scheduler scheduler;
//    @Scheduled(cron = "0 0 0/1 * * ?")

    public void findTop20SharedRecordHourly() {

        long now = System.currentTimeMillis();
        Date endDateTime = new Date(now);
        Date startDateTime = new Date(now - 3600000);//past 1 hour

        TypedAggregation<ActivityLog> aggregation = newAggregation(
                ActivityLog.class,
                match(Criteria.where("activity_type").in(Constants.UserActivityConstant.INTRA_SHARE_RECORD, Constants.UserActivityConstant.SOCIAL_SHARE_RECORD)),
                match(Criteria.where("activity.activity_ts").gte(startDateTime)),
                match(Criteria.where("activity.activity_ts").lte(endDateTime)),
                group("recordIdentifier").count().as("count"),
                project("count").and("recordIdentifier").previousOperation(),
                sort(DESC, "count"),
                limit(20)
        );
        AggregationResults<Record> results = mongoTemplate.aggregate(aggregation, Record.class);
        List<Record> mappedRecords = results.getMappedResults();

        TopSharedResource topSharedResource = new TopSharedResource(startDateTime, endDateTime, mappedRecords);
        miningRepository.save(topSharedResource);
    }

    /**
     *
     * @param timeRange {d = past 24 hours, w = past week from current time, m =
     *                  past month, y for past year}
     * @return outputMap of data and trends
     */
//    public HashMap<String, Object> findTop10SharedRecord(String timeRange) {
//        long now = System.currentTimeMillis();
//        long startTime = 0L;
//        long millisInDay = 86400000L, millisInHour = 3600000L;
//        Date endDateTime = new Date(now);
//        Date startDateTime = new Date(now - millisInDay);//past 24 hour
//        long timeStep = 0L;
//        switch (timeRange) {
//            case "d":
//                startTime = now - millisInDay;
//                startDateTime = new Date(startTime);//past 24 hour
//                timeStep = millisInHour;
//                break;
//            case "w":
//                startTime = now - (millisInDay * 7L);
//                startDateTime = new Date(startTime);//past week
//                timeStep = millisInDay;
//                break;
//            case "m":
//                startTime = now - (millisInDay * 30L);
//                startDateTime = new Date(startTime);//past month
//                timeStep = millisInDay;
//                break;
//            case "y":
//                startTime = now - (millisInDay * 30L * 12L);
//                startDateTime = new Date(startTime);//past Year
//                timeStep = millisInDay * 30L;
//                break;
//            default:
//                startTime = now - (millisInDay);
//                startDateTime = new Date(now - (millisInDay));//past 24 hour
//                timeStep = millisInHour;
//                break;
//        }
//        TypedAggregation<ActivityLog> aggregation = newAggregation(
//                ActivityLog.class,
//                match(Criteria.where("activity_type").in(Constants.UserActivityConstant.INTRA_SHARE_RECORD,Constants.UserActivityConstant.SOCIAL_SHARE_RECORD)),
//                //                match(Criteria.where("activity_type").is(Constants.UserActivityConstant.SOCIAL_SHARE_RECORD)),
//                match(Criteria.where("activity.activity_ts").gte(startDateTime)),
//                match(Criteria.where("activity.activity_ts").lte(endDateTime)),
//                group("recordIdentifier").count().as("count"),
//                project("count").and("recordIdentifier").previousOperation(),
//                sort(DESC, "count"),
//                limit(10)
//        );
//        AggregationResults<Record> results = mongoTemplate.aggregate(aggregation, Record.class);
//        List<Record> mappedRecords = results.getMappedResults();
//
//        HashMap<String, Object> trendList = new HashMap<>();
//        HashMap<String, Object> outputMap = new HashMap<>();
//        List recordIdentifiers = new ArrayList<>();
//        for (Record mappedRecord : mappedRecords) {
//            trendList.put(mappedRecord.getRecordIdentifier(), getShareTrendValues(mappedRecord.getRecordIdentifier(), startTime, now, timeStep));
//            recordIdentifiers.add(mappedRecord.getRecordIdentifier());
//        }
//        outputMap.put("data", mappedRecords);
//        outputMap.put("trendsData", trendList);
//        outputMap.put("info", resourceInfoService.getResourceInfo(recordIdentifiers));
//        return outputMap;
//    }
    public Map<String, Object> findTop10SharedRecordDefault(Date startDateTime, Date endDateTime, long startTime, long timeStep, long now) {
        TypedAggregation<ActivityLog> aggregation = newAggregation(
                ActivityLog.class,
                match(Criteria.where("activity_type").in(Constants.UserActivityConstant.INTRA_SHARE_RECORD, Constants.UserActivityConstant.SOCIAL_SHARE_RECORD)),
                //                match(Criteria.where("activity_type").is(Constants.UserActivityConstant.SOCIAL_SHARE_RECORD)),
                match(Criteria.where("activity.activity_ts").gte(startDateTime)),
                match(Criteria.where("activity.activity_ts").lte(endDateTime)),
                group("recordIdentifier").count().as("count"),
                project("count").and("recordIdentifier").previousOperation(),
                sort(DESC, "count"),
                limit(10)
        );
        AggregationResults<Record> results = mongoTemplate.aggregate(aggregation, Record.class);
        List<Record> mappedRecords = results.getMappedResults();

        HashMap<String, Object> trendList = new HashMap<>();
        HashMap<String, Object> map = new HashMap<>();
        List recordIdentifiers = new ArrayList<>();
        for (Record mappedRecord : mappedRecords) {
            trendList.put(mappedRecord.getRecordIdentifier(), scheduler.getShareTrendValues(mappedRecord.getRecordIdentifier(), startTime, now, timeStep));
            recordIdentifiers.add(mappedRecord.getRecordIdentifier());
        }
        map.put("data", mappedRecords);
        map.put("trendsData", trendList);
        map.put("info", resourceInfoService.getResourceInfo(recordIdentifiers));
        return map;
    }

    public Map<String, Object> findTop10SharedRecord(String timeRange) {
        long now = System.currentTimeMillis();
        long startTime;
        long millisInDay = 86400000L, millisInHour = 3600000L;
        Date endDateTime = new Date(now);
        Date startDateTime;
        long timeStep;
        Map<String, Object> outputMap;
        switch (timeRange) {
            case "d":
                startTime = now - millisInDay;
                startDateTime = new Date(startTime);//past 24 hour
                outputMap = getOutputMapOfSharedRecord(startDateTime, endDateTime, startTime, millisInHour, now, "recordLast24Hr");
                break;
            case "w":
                startTime = now - (millisInDay * 7L);
                startDateTime = new Date(startTime);//past week
                outputMap = getOutputMapOfSharedRecord(startDateTime, endDateTime, startTime, millisInDay, now, "recordLast1M7D");
                break;
            case "m":
                startTime = now - (millisInDay * 30L);
                startDateTime = new Date(startTime);//past month
                outputMap = getOutputMapOfSharedRecord(startDateTime, endDateTime, startTime, millisInDay, now, "recordLast1M7D");
                break;
            case "y":
                startTime = now - (millisInDay * 30L * 12L);
                startDateTime = new Date(startTime);//past Year
                timeStep = millisInDay * 30L;
                outputMap = getOutputMapOfSharedRecord(startDateTime, endDateTime, startTime, timeStep, now, "recordLast1M7D");
                break;
            default:
                startTime = now - (millisInDay);
                startDateTime = new Date(now - (millisInDay));//past 24 hour
                outputMap = getOutputMapOfSharedRecord(startDateTime, endDateTime, startTime, millisInHour, now, "recordLast1Year");
                break;
        }
        System.out.println("size " + outputMap.size());
        return outputMap;
    }

    private Map<String, Object> getOutputMapOfSharedRecord(Date startDateTime, Date endDateTime, long startTime, long timeStep, long now, String collectionName) {
        List<RecordShareCollection> recordShareCollections;
        Map<String, Object> outputMap = new HashMap<>();
        try {
            recordShareCollections = mongoTemplate.findAll(RecordShareCollection.class, collectionName);
            System.out.println(" size 2 " + recordShareCollections.size());
            if (recordShareCollections.get(0).getMappedRecords().isEmpty()) {
                outputMap = findTop10SharedRecordDefault(startDateTime, endDateTime, startTime, timeStep, now);
            } else {
                outputMap.put("data", recordShareCollections.get(0).getMappedRecords());
                outputMap.put("trendsData", recordShareCollections.get(0).getTrendList());
                outputMap.put("info", recordShareCollections.get(0).getInfo());
            }
        } catch (Exception ex) {
            System.out.println("ex " + ex.getLocalizedMessage());
            outputMap = findTop10SharedRecordDefault(startDateTime, endDateTime, startTime, timeStep, now);
        }
        return outputMap;
    }

//    @Scheduled(cron = "0 0 0/1 * * ?")
    public void findTrendingSearchHourly() {

        long now = System.currentTimeMillis();
        System.out.println("Trending Search >>> @" + now);
        Date endDateTime = new Date(now);
        Date startDateTime = new Date(now - 3600000L);//past 1 hour

        TypedAggregation<ActivityLog> aggregation = newAggregation(
                ActivityLog.class,
                match(Criteria.where("activity_type").in(Constants.UserActivityConstant.INTRA_SHARE_RECORD, Constants.UserActivityConstant.SOCIAL_SHARE_RECORD)),
                match(Criteria.where("activity.activity_ts").gte(startDateTime)),
                match(Criteria.where("activity.activity_ts").lte(endDateTime)),
                group("recordIdentifier").count().as("count"),
                project("count").and("recordIdentifier").previousOperation(),
                sort(DESC, "count"),
                limit(10)
        );
        AggregationResults<Record> results = mongoTemplate.aggregate(aggregation, Record.class);
        List<Record> mappedRecords = results.getMappedResults();

        TopSharedResource topSharedResource = new TopSharedResource(startDateTime, endDateTime, mappedRecords);
        miningRepository.save(topSharedResource);
    }

    public List<ActivityLog> fetchRecordsBetween(Date startDateTime, Date endDateTime) {
        List<ActivityLog> activities = activityLogRepository.findByActivityTypeAndActivityActivityTsBetween(Constants.UserActivityConstant.INTRA_SHARE_RECORD, startDateTime, endDateTime);
        return activities;
    }

    public Map<String, Object> fetchSearchTrendsBetween() {
        List<TrendingSearchCollection> trendingSearchCollections = mongoTemplate.findAll(TrendingSearchCollection.class);
        Map<String, Object> searchTrendMap = new HashMap<>();
        searchTrendMap.put("data", trendingSearchCollections.get(0).getMappedRecords());
        searchTrendMap.put("trends", trendingSearchCollections.get(0).getTrendList());
        return searchTrendMap;
    }

    public List<Integer> getRandTrends() {
        List<Integer> trends = new ArrayList<>();
        for (int i = 0; i < 24; i++) {
            Random r = new Random();
            int Low = 80;
            int High = 100;
            int Result = r.nextInt(High - Low) + Low;
            trends.add(Result);
        }

        return trends;
    }


}
