package in.gov.nvli.analytics.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 *
 * @author Sujata Aher
 * @author Priya More
 * @since 1
 * @version 1
 */
public class Constants {

    /**
     * NVLI portal table prefix.
     *
     * @since 1
     */
    public final static String PORTAL_TABLE_PREFIX = "pr_";
    /**
     * NVLI Portal Table Names
     */
    public final static String PORTAL_USER_TABLE_NAME = "user";
    public final static String PORTAL_CATALOG_NAME = "test_nvli";
    public final static String PORTAL_WIDGET_TABLE_NAME = "widget";

    /**
     * Properties for user activity logging
     */
    public static final class UserActivities {

        public static final int LOGIN = 1;
        public static final int PASSWORD_CHANGE = 2;
        public static final int SIGNUP = 3;
        public static final int ACTIVATE_USER = 4;
        public static final int DEACTIVATE_USER = 5;
        public static final int DELETE_USER = 6;
        public static final int EDIT_USER_DETAILS = 7;
        public static final int UPDATE_PROFILE = 8;
        public static final int INVITE_USER = 9;
        public static final int LIKE_RECORD = 10;
        public static final int SOCIAL_SHARE_RECORD = 11;
        public static final int INTRA_SHARE_RECORD = 12;
        public static final int EMAIL_RECORD = 13;
        public static final int DOWNLOAD_RECORD = 14;
        public static final int EXPORT_RECORD = 15;
        public static final int ADD_TO_LIST = 16;
        public static final int REMOVE_FROM_LIST = 17;
        public static final int CREATE_LIST = 18;
        public static final int DELETE_LIST = 19;
        public static final int ADD_REVIEW = 20;
        public static final int UPDATE_REVIEW = 21;
        public static final int DELETE_REVIEW = 22;
        public static final int RATE_RECORD = 23;
        public static final int KEYWORD_SEARCHED = 24;
        public static final int LINK_VISITED = 25;
        public static final int ADD_TAG = 26;
        public static final int REMOVE_TAG = 27;
        public static final int FACEBOOK_LOGIN = 28;
        public static final int TWITTER_LOGIN = 31;
        public static final int GOOGLE_LOGIN = 32;
        public static final int LINKEDIN_LOGIN = 33;
        public static final int GITHUB_LOGIN = 34;
        public static final int ADD_EBOOK = 29;
        public static final int SAVED_LINK = 35;
    }

    /**
     * These are the actual collection names of database.
     */
    public static final class CollectionNames {

        public static final String USER_ACTIVITY_LOG = "activityLog";
    }
    /**
     * property for deleted/non deleted
     */
    public static final int DELETED = 1;
    public static final int NON_DELETED = 0;
    /**
     * end of property for deleted/non deleted
     */
    /**
     * property for user saved/unsaved search
     */
    public static final int SAVED_SEARCH = 1;
    public static final int UNSAVED_SEARCH = 0;
    /**
     * end of property for user saved/unsaved search
     */
    /**
     * property for user active/inactive status i.e email verified or not
     */
    public static final byte USER_ACTIVE = 1;
    public static final byte USER_INACTIVE = 0;
    /**
     * end of property for user active/inactive status i.e email verified or not
     */
    /**
     * property for user deleted/non-deleted status
     */
    public static final byte USER_DELETED = 1;
    public static final byte USER_NOT_DELETED = 0;
    /**
     * end of property for user deleted/non-deleted status
     */
    /**
     * property for user activated/deactivated user
     */
    public static final byte USER_DEACTIVATED = 0;
    public static final byte USER_ACTIVATED = 1;
    /**
     * end of property for user activated/deactivated user
     */
    /**
     * property for list visibility
     */
    public static final int PUBLIC_LIST_ID = 0;
    public static final int PRIVATE_LIST_ID = 1;
    public static final String PUBLIC_LIST_LABEL = "Public";
    public static final String PRIVATE_LIST_LABEL = "Private";
    /**
     * end of property for list visibility
     */
    /**
     * property for user invitation_accepted or not
     */
    public static final int INVITE_NOT_ACCEPTED = 0;
    public static final int INVITE_ACCEPTED = 1;
    public static final String INVITE_NOT_ACCEPTED_STR = "Pending";
    public static final String INVITE_ACCEPTED_STR = "Registered";
    /**
     * end of property for user invitation_accepted or not
     */
    public static final String REG_EXP_FOR_EXTENSION = "\\.(?=[^\\.]+$)";

    /**
     * User Roles
     */
    public static final class UserRole {

        public static final String ANONYMOUS_USER = "ANONYMOUS";
        public static final String ADMIN_USER = "ADMIN_USER";
        public static final String GENERAL_USER = "GENERAL_USER";
        public static final long GENERAL_USER_ID = 1;
        public static final String LIB_USER = "LIB_USER";
        public static final String MOC_USER = "MOC_USER";
        public static final String TRANSLATOR_USER = "TRANSLATOR_USER";
        public static final String CURATOR_USER = "CURATOR_USER";
        public static final String CROWD_MANAGER = "CROWD_MANAGER";
    }
    public static final String MARKER_NAME = "[a-zA-Z0-9.? _-]+";
    public static final String TIME_CODE = "([0-9]+):([0-5][0-9]):([0-5][0-9]).([0-9]{1,3})";

    /**
     * Meta-data type constants
     */
    public static final class MetadataStandardType {

        public final static String MARC21 = "marc21";
        public final static String DUBLIN_CORE = "dublin-core";
        public final static String ARTIST_PROFILE = "artist-profile";
        public final static String INDIAN_RECIPES = "indian-recipes";
    }

    /**
     * Tag Type
     */
    public static final class TagType {

        public static final String UDC = "UDCTagging";
        public static final String CUSTOM = "CustomTagging";
        public static final String MARKER = "TimecodeMarking";
    }

    /**
     * AJAX Response Status CODES
     */
    public static final class AjaxResponseCode {

        public static final String SUCCESS = "success";
        public static final String FAILED = "failed";
        public static final String ACCESS_DENIED = "access_denied";
        public static final String ERROR = "error";
        public static final String INPUT_ERROR = "input_error";
    }

    /**
     * Content Type
     */
    public static final class ContentType {

        public static final String AUDIO = "audio";
        public static final String VIDEO = "video";
        public static final String JPEG = "jpeg";
        public static final String TIFF = "tif";
        public static final String DOCUMENT = "document";
    }
    public static final Map GROUP_WITH_CLASS = new HashMap<String, String>() {

        {
            put("Management", "mgt");
            put("Agriculture", "agri");
            put("Technology", "tech");
            put("Science", "sci");
            put("Economics", "economy");
            put("University", "university");
            put("Electronic Thesis and Dissertations", "");
            put("Library", "");
            put("Audio-Video", "aud_vis");
            put("Astronomy", "astro");
            put("Science", "sci");
            put("Health", "health");
            put("Environment", "env");
            put("Museum Archelogy", "mus_arc");
            put("Engineering", "engg");
            put("Medicine", "med");
            put("Medical", "med");
            put("Literature", "litr");
            put("Library and Information Science", "library");
            put("Biology", "bio");
            put("Aerospace", "aero");
            put("Scientific and Industrial Research", "sci_indus");
            put("National Museums", "nat_mus");
            put("Astrophysics", "astro");
            put("Archaeological Survey of India", "asi");
            put("National Gallery of Modern Arts", "ngma");

        }
    };
    public static final String[] ESCAPE_URLS_FROM_REDIRECTION = {
        "/auth/"
    };
    public final static String STRING_SEPARATOR = "&|&";
    /**
     * ranking rules
     */
    public final static String NVLI_RL_1 = "nvli_rl_1";
    public final static String NVLI_RL_2 = "nvli_rl_2";
    public final static String NVLI_RL_3 = "nvli_rl_3";
    public final static String NVLI_RL_4 = "nvli_rl_4";
    public final static String NVLI_RL_5 = "nvli_rl_5";
    public final static String NVLI_RL_6 = "nvli_rl_6";

    public static final class Notification {

        public static final String DEFAULT_NOTIFICATION_ENDPOINT = "";
        /**
         * Please add `user id` at the end i.e.
         * `Constants.Notification.DEFAULT_NOTIFICATION_URL + user.getId()`
         */
        public static final String DEFAULT_NOTIFICATION_URL = "/notifs/user-notifications/";

    }
    public static final String DEFAULT_THEME_BROKER_URL = "/themeBroker/themepush";
    public final static String METS = "mets";
    public final static String ORE = "ore";

    public static final class SocialProviders {

        public static final String FACEBOOK = "facebook";
        public static final String TWITTER = "twitter";
        public static final String LINKEDIN = "linkedin";
        public static final String GITHUB = "github";
        public static final String GOOGLE = "google";
    }

    public final static String ASSIGNED_ID = "assign-id";
    public final static String CURATION_STATUS = "curation-status";
    public final static String CURATED = "curated";
    public final static String UNCURATED = "uncurated";

    public static final String FEED_URL_EXCEPTIONS[] = new String[]{
        "my.aol.com",
        "bloglines.com",
        "my.msn.com",
        "pageflakes.com",
        "netvibes.com",
        "rojo.com",
        "protopage.com",
        "live.com",
        "newsgator.com",
        "add.my.yahoo.com",
        "fusion.google.com"
    };

    public static final int CONNECTION_TIMEOUT = 5000;

    public static final String RESOURCE_TYPE_MIT = "Resource with Metadata";
    public static final String RESOURCE_TYPE_HARVEST = "Harvest";
    public static final String RESOURCE_TYPE_WEB_CRAWLER = "Web crawler";
    public static final String RESOURCE_TYPE_ENEWS = "e-News";
    public static final String RESOURCE_TYPE_BLOG_CODE = "BLOG";

    public static final class InvitationStatus {

        public static final int PENDING = 0;
        public static final int CANCELLED = 2;
        public static final int ACCEPTED = 1;

    }

    public static final class UserActivityConstant {

        public static final String LOGIN = "login";
        public static final String PASSWORD_CHANGE = "changedPassword";
        public static final String SIGNUP = "signup";
        public static final String ACTIVATE_USER = "activateUser";
        public static final String DEACTIVATE_USER = "deactivateUser";
        public static final String DELETE_USER = "deleteUser";
        public static final String EDIT_USER_DETAILS = "editUserDetails";
        public static final String UPDATE_PROFILE = "updateProfile";
        public static final String INVITE_USER = "inviteUser";
        public static final String LIKE_RECORD = "likeRecord";
        public static final String SOCIAL_SHARE_RECORD = "socialShareRecord";
        public static final String RATE_RECORD = "rateRecord";
        public static final String FACEBOOK_LOGIN = "facebookLogin";
        public static final String TWITTER_LOGIN = "twitterLogin";
        public static final String GOOGLE_LOGIN = "googleLogin";
        public static final String LINKEDIN_LOGIN = "linkedinLogin";
        public static final String GITHUB_LOGIN = "githubLogin";
        public static final String ADD_EBOOK = "addEbook";
        public static final String DELETE_EBOOK = "deleteEbook";
        public static final String INTRA_SHARE_RECORD = "intraShareRecord";
        public static final String ADD_TAG = "addTag";
        public static final String REMOVE_TAG = "removeTag";
        public static final String KEYWORD_SEARCHED = "userSearchPhrases";
        public static final String EMAIL_RECORD = "emailrecord";
        public static final String DOWNLOAD_RECORD = "downloadRecord";
        public static final String EXPORT_RECORD = "exportRecord";
        public static final String ADD_REVIEW = "addReview";
        public static final String UPDATE_REVIEW = "updateReview";
        public static final String DELETE_REVIEW = "deleteReview";
        public static final String REVIEW = "Review";
        public static final String ADD_TO_LIST = "addToList";
        public static final String REMOVE_FROM_LIST = "removeFromList";
        public static final String CREATE_LIST = "createList";
        public static final String DELETE_LIST = "deleteList";
        public static final String LINK_VISITED = "linkVisited";
        public static final String SAVED_PHRASE = "userSavedPhrases";
    }
    public static final Set<String> ENEWS_CATEGORY_SET = new HashSet<String>() {
        {
            add("en");
            add("hi");
            add("ml");
            add("ta");
            add("te");
        }
    };
}
