/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.analytics.util;

import org.springframework.http.HttpHeaders;
import org.springframework.security.crypto.codec.Base64;

/**
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in, rabidassanjay@gmail.com>
 */
public class Helper {
        /**
     * method used to call MIT Web Service Authentication
     *
     * @return HttpHeaders Object
     */
    public static HttpHeaders mitWebserviceAthentication() {
        String plainCreds = "MetadataIntegratorUser:mit_user@123#";
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encode(plainCredsBytes);
        String base64Creds = new String(base64CredsBytes);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Creds);
        return headers;
    }
}
