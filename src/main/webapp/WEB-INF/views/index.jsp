<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<style>
    .text-center{
        text-align: center;
    }
    .graph-container{
        height: 300px;
        width: 500px;
        margin: 20px auto;
        border: 2px solid #ddd;
        border-radius: 8px;
        padding: 15px;
    }
</style>
<div class="container text-center">
    <h1>National Virtual Library of India</h1>
    <h3><span class="fa fa-bar-chart-o" style="color:#f7a35c;"></span> A N A L Y T I C S &mdash; E N D P O I N T</h3>
    <div class="graph-container"></div>
</div>
<script src="${context}/components/jquery/jquery.min.js"></script>
<script src="${context}/components/highcharts/highcharts.js"></script>
<script>
    $(document).ready(function(){
        $(".graph-container").highcharts({
            title: {
                    text: 'Analytics'
                },
                yAxis: {
                    title: {
                        text: 'Count'
                    },
                    tickInterval: 1
                },
                xAxis: {
                    title: {
                        text: "Data"
                    },
                    categories: [1,2,3,4,5]

                },
                chart: {
                    type: 'spline'
                },
                series: [
                    {
                       data : [1,3,4,9,1]
                    },
                    {
                       data : [11,3,4,4,1]
                    },
                    {
                       data : [16,33,4,9,1]
                    },
                    {
                       data : [13,30,4,9,1]
                    },
                    {
                       data : [1,3,64,89,91]
                    }
                ]
        });
    });
</script>