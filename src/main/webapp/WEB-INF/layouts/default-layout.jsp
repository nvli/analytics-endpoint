<%--
    Document   : default-layout
    Created on : Apr 1, 2016, 5:27:19 PM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
    Author     : Priya Bhalerao
    Author     : Madhuri Dhone
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" type="image/png" href="${context}/images/trend.png" sizes="16x16">
        <link rel="icon" type="image/png" href="${context}/images/trend.png" sizes="32x32">
        <link rel="stylesheet" href="${context}/components/font-awesome/css/font-awesome.min.css">
        <title><tiles:insertAttribute name="title"/></title>
    </head>
    <body>
        <div class="container">
            <tiles:insertAttribute name="body" ignore="true"/>
        </div>
    </body>
</html>
